
/*
	You declare function by using:
	1. function keyword
	2. function name
	3. open/close parenthesis
	4. open/close curly braces
*/
function  sayHello(){
	console.log('Hello there!')
}

// You can invoke a function by calling its function name and including the parenthesis
sayHello();

// You can assign a function to a variable. The function name would not be required.
let sayGoodbye = function(){
	console.log('Goodbye!');
};

// You can invoke a functon inside a variable by using the viariable name
sayGoodbye();

// You can also re-assign a function as a new value of a variable
sayGoodbye = function (){
	console.log('Au Revoir!');
};

sayGoodbye();

// Declaring a constant variable with a function as a value will not allow that function to be changed or re-assigned
const sayHelloInJapanese = function(){
	console.log('Ohayo!');
}

/*sayHelloInJapanese = function(){
	console.log('Kamusta!');
} -- error since const*/

sayHelloInJapanese();

// local scope and global scope

/*
	Global - is available outside the function
	Local - within or inside the function
*/

// Global Scope - You can use a variable inside the function if the variable is declared outside of it



function doSomethingRandom(){
	let action = 'Run';
	console.log(action);
}

doSomethingRandom();


// Local/Function scope - You cannot use a variable outside of a function if it is within the function scope/curly braces

function doSomethingRandom(){
	let action = 'Run';
	
}

// console.log(action);


// console.clear();

// You can nest function inside of a function as long as you invoke the child function within the scope of the parent function
function viewProduct(){

	console.log('Viewing a product');

	function addToCart(){
		console.log('Added product to cart');
	};
	addToCart();
};

viewProduct();

// console.clear();


// Alert function is a built-in javascript function where we can show alerts to the user
function singASong(){
	alert('La la la');
}

singASong();

// Any statement like 'console.log' will run only after the alert has been closed if it is invoked below of the alert function
console.log('Clap clap clap');

// Prompt is a build-in javascript function that we can use to 
function enterUserName(){
	// let userName = prompt('Enter your username');
	let userName = prompt('Enter your username');
	console.log(userName);
}

enterUserName();

// You can use a prompt outside of a function. Make syre to output prompt value by assigning to a variable and using a console.log
let userAge = prompt('Enter your age');
console.log(userAge);

// console.log(prompt('Enter your age'));


/*(function() {
    var name = [];
    for (var i = 0; i < 3; i++) {           // Note 1
        name[i] = prompt('Enter your name'); // Note 2
    }
    console.log(name);
})();*/